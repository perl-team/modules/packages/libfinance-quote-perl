Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Finance::Quote
Upstream-Contact: Erik Colson <eco@ecocode.net>
Source: https://metacpan.org/release/Finance-Quote

Files: *
Copyright: 1998 1999 Linas Vepstas <linas@linas.org>
           1998 Dj Padzensky <djpadz@padz.net>
           1998, 1999 Linas Vepstas <linas@linas.org>
           2000 Brent Neal <brentn@users.sourceforge.net>
           2000 Keith Refson <Keith.Refson@earth.ox.ac.uk>
           2000 Paul Fenwick <pjf@cpan.org>
           2000 Volker Stuerzl <volker.stuerzl@gmx.de>
           2000 Yannick LE NY <y-le-ny@ifrance.com>
           2000-2001 Brent Neal <brentn@users.sourceforge.net>
           2000-2004 Paul Fenwick <pjf@cpan.org>
           2001 James Treacy (TD Waterhouse support)
           2001 Leigh Wedding <leigh.wedding@telstra.com>
           2001 Rob Sessink <rob_ses@users.sourceforge.net>
           2001 Tobias Vancura <tvancura@altavista.net>
           2002 Rainer Dorsch <rainer.dorsch@informatik.uni-stuttgart.de>
           2003 Tomas Carlsson <tc@tompa.nu>
           2004 Frank Mori Hess <fmhess@users.sourceforge.net>
           2005 Morten Cools <morten@cools.no>
           2006 Dominique Corbex <domcox@sourceforge.net>
           2006 Klaus Dahlke <klaus.dahlke@gmx.de>
           2007 Jan Willamowius <jan@willamowius.de>
           2008 Bernard Fuentes <bernard.fuentes@gmail.com>
           2008 Erik Colson (isoTime)
           2008 Stephan Ebelt <stephan.ebelt@gmx.de>
           2008-2009 Erik Colson <eco@ecocode.net>
           2010 Martin Sadler
           2012 Christopher Hill
           2014 Chris Good <chris.good@@ozemail.com.au>
           2018 Diego Marcolungo
           2018 Jean-Marie Pacquet <jmpacquet@sourceforge.net>
           2019 Jalon Avens
           2019 Manuel Friedli <manuel@fritteli.ch>
           2020 Caleb Begly <calebbegly@gmail.com>
           2022 Andre Joost <andrejoost@gmx.de>
           2023-2024 Bruce Schuck <bschuck@asgard-systems.com>
           2023 Stephan Gambke <s7eph4n@gmail.com>
License: GPL-2+

Files: debian/*
Copyright: 2001-2003 Ross Peachey <rcp@debian.org>
           2006 Don Armstrong <don@debian.org>
           2006-2012 Bart Martens <bartm@knars.be>
           2013 Jackson Doak <noskcaj@ubuntu.com>
           2014-2025 Sébastien Villemot <sebastien@debian.org>
           2017-2025 gregor herrmann <gregoa@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 2 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see
 <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.
